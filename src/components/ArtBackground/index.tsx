import React from 'react'
import styles from './styles.module.scss'

const ArtBackground = ({ image, alt, height }) => {
  return (
    <div
      className={styles['art-background']}
      style={{
        ['--height' as any]: height + 'px'
      }}
    >
      <div className={styles['image-container']}>
        <img src={image} alt={alt} className={styles.image} />
      </div>
    </div>
  )
}

export default ArtBackground
